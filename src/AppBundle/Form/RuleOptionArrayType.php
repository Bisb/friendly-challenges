<?php

namespace AppBundle\Form;

use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;

class RuleOptionArrayType extends RuleOptionType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('options', TextType::class)
            ->add('random')
            ->add('randomCountMin', IntegerType::class, ['constraints' => [new GreaterThanOrEqual(['value' => 0])]])
            ->add('randomCountMax', IntegerType::class, ['constraints' => [new GreaterThanOrEqual(['value' => 0])]]);

        $builder->get('options')
            ->addModelTransformer(
                new CallbackTransformer(
                    function ($tagsAsArray) {
                        return implode(', ', $tagsAsArray);
                    },
                    function ($tagsAsString) {
                        $array = explode(',', $tagsAsString);
                        foreach ($array as &$item) {
                            $item = trim($item);
                        }

                        return $array;
                    }
                )
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'AppBundle\Entity\RuleOptionArray',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_ruleoptionarray';
    }


}
