<?php

namespace AppBundle\Form;

use AppBundle\Entity\ChallengeParticipation;
use AppBundle\Entity\Player;
use AppBundle\Entity\Rule;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Count;

class ChallengeParticipationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var ChallengeParticipation $entity */
        $entity = $options['data'];

        $builder
            ->add('challenge')
            ->add(
                'rule',
                EntityType::class,
                [
                    'multiple'      => true,
                    'label'         => 'Rules',
                    'mapped'        => false,
                    'constraints'   => [new Count(['min' => 1])],
                    'class'         => Rule::class,
                    'query_builder' => function (EntityRepository $er) use ($entity) {
                        return $er->createQueryBuilder('rule')
                            ->where('rule.game = :game')
                            ->setParameter('game', $entity->getChallenge()->getGame());
                    },
                ]
            )
            ->add(
                'players',
                EntityType::class,
                [
                    'required'      => false,
                    'class'         => Player::class,
                    'multiple'      => true,
                    'query_builder' => function (EntityRepository $er) use ($entity) {
                        return $er->createQueryBuilder('player')
                            ->join('player.challenges', 'challenges')
                            ->where('challenges = :challenge')
                            ->setParameter('challenge', $entity->getChallenge());
                    },
//                    'constraints'   => [
//                        new Count(['min' => 1]),
//                    ],
                ]
            )
            ->add('random', CheckboxType::class, ['label' => 'Randomize players', 'required' => false]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'AppBundle\Entity\ChallengeParticipation',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_challengeparticipation';
    }


}
