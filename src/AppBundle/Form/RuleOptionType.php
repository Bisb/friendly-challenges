<?php

namespace AppBundle\Form;

use AppBundle\Entity\RuleOption;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RuleOptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('successPoints')
            ->add('failurePoints')
            ->add('defaultPoints')
            ->add('inversedSuccess')
            ->add('message');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => RuleOption::class,
            ]
        );
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_rule_option_type';
    }
}
