<?php

namespace AppBundle\Form;

use AppBundle\Entity\Rule;
use AppBundle\Entity\RuleOptionArray;
use AppBundle\Entity\RuleOptionBoolean;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RuleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Rule $entity */
        $entity = $options['data'];

        $builder
            ->add('name')
            ->add('game')
            ->add('description');

        if ($entity->getRuleOption() instanceof RuleOptionArray) {
            $builder->add('ruleOption', RuleOptionArrayType::class);
        } elseif ($entity->getRuleOption() instanceof RuleOptionBoolean) {
            $builder->add('ruleOption', RuleOptionBooleanType::class);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => Rule::class,
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_rule';
    }


}
