<?php

namespace AppBundle\DBAL;


class EnumChallengeStateType extends EnumType
{
    const CHALLENGE_STATE_PREPARING = 'preparing';
    const CHALLENGE_STATE_STARTED = 'started';
    const CHALLENGE_STATE_FINISHED = 'finished';
    const CHALLENGE_STATE_CANCELLED = 'cancelled';

    protected $name = 'enum_challenge_state';
    protected $values = array(
        self::CHALLENGE_STATE_PREPARING,
        self::CHALLENGE_STATE_STARTED,
        self::CHALLENGE_STATE_FINISHED,
        self::CHALLENGE_STATE_CANCELLED,
    );
}