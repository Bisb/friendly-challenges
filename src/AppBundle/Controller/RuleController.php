<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Rule;
use AppBundle\Entity\RuleOptionArray;
use AppBundle\Entity\RuleOptionBoolean;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Rule controller.
 *
 * @Route("rule")
 */
class RuleController extends Controller
{
    /**
     * Lists all rule entities.
     *
     * @Route("/", name="rule_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $rules = $em->getRepository('AppBundle:Rule')->findAll();

        return $this->render(
            'rule/index.html.twig',
            array(
                'rules' => $rules,
            )
        );
    }

    /**
     * Creates a new rule entity.
     *
     * @Route("/new/{ruleOption}", name="rule_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, $ruleOption)
    {
        $rule = new Rule();
        if ($ruleOption == 'array') {
            $ruleOption = new RuleOptionArray();
        } elseif ($ruleOption == 'boolean') {
            $ruleOption = new RuleOptionBoolean();
        }

        $rule->setRuleOption($ruleOption);
        $form = $this->createForm('AppBundle\Form\RuleType', $rule);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($rule);
            $em->flush();

            return $this->redirectToRoute('rule_show', array('id' => $rule->getId()));
        }

        return $this->render(
            'rule/new.html.twig',
            array(
                'rule'             => $rule,
                'rule_option_type' => get_class($ruleOption),
                'form'             => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a rule entity.
     *
     * @Route("/{id}", name="rule_show")
     * @Method("GET")
     */
    public function showAction(Rule $rule)
    {
        $deleteForm = $this->createDeleteForm($rule);

        return $this->render(
            'rule/show.html.twig',
            array(
                'rule'        => $rule,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to edit an existing rule entity.
     *
     * @Route("/{id}/edit", name="rule_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Rule $rule)
    {
        $deleteForm = $this->createDeleteForm($rule);
        $editForm = $this->createForm('AppBundle\Form\RuleType', $rule);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('rule_edit', array('id' => $rule->getId()));
        }

        return $this->render(
            'rule/edit.html.twig',
            array(
                'rule'        => $rule,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a rule entity.
     *
     * @Route("/{id}/delete", name="rule_delete")
     */
    public function deleteAction(Rule $rule)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($rule);
        $em->flush();

        return $this->redirectToRoute('rule_index');
    }

    /**
     * Creates a form to delete a rule entity.
     *
     * @param Rule $rule The rule entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Rule $rule)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('rule_delete', array('id' => $rule->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
