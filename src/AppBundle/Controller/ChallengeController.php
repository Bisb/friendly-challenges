<?php

namespace AppBundle\Controller;

use AppBundle\DBAL\EnumChallengeStateType;
use AppBundle\Entity\Challenge;
use AppBundle\Entity\ChallengeParticipation;
use AppBundle\Entity\ChallengeParticipationValue;
use AppBundle\Entity\RuleOptionArray;
use AppBundle\Form\ChallengeParticipationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Challenge controller.
 *
 * @Route("challenge")
 */
class ChallengeController extends Controller
{
    /**
     * Lists all challenge entities.
     *
     * @Route("/", name="challenge_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $challenges = $em->getRepository('AppBundle:Challenge')->findAll();

        return $this->render(
            'challenge/index.html.twig',
            array(
                'challenges' => $challenges,
            )
        );
    }

    /**
     * Creates a new challenge entity.
     *
     * @Route("/new", name="challenge_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $challenge = new Challenge();
        $form = $this->createForm('AppBundle\Form\ChallengeType', $challenge);
        $form->remove('rules');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($challenge);
            $em->flush();

            return $this->redirectToRoute('challenge_edit', array('id' => $challenge->getId()));
        }

        return $this->render(
            'challenge/new.html.twig',
            array(
                'challenge' => $challenge,
                'form'      => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a challenge entity.
     *
     * @Route("/{id}", name="challenge_show")
     * @Method("GET")
     */
    public function showAction(Request $request, Challenge $challenge)
    {
        return $this->render('challenge/show.html.twig', ['challenge' => $challenge]);
    }

    /**
     * @Route("/{id}/start", name="challenge_start", requirements={"id" = "\d+"})
     */
    public function startAction(Challenge $challenge)
    {
        $em = $this->getDoctrine()->getManager();
        $challenge->setState(EnumChallengeStateType::CHALLENGE_STATE_STARTED);
        $challenge->setDateStarted(new \DateTime());

        foreach ($challenge->getChallengeParticipations() as $participation) {
            $players = $participation->getPlayers()->toArray();
            if ($participation->getRandom()) {
                shuffle($players);
                $players = array_slice($players, 0, rand(1, count($players)));
            }

            foreach ($players as $player) {
                $participationValue = new ChallengeParticipationValue();
                $participationValue->setPlayer($player);
                $participationValue->setChallengeParticipation($participation);
                if ($participation->getRule()->getRuleOption() instanceof RuleOptionArray) {
                    /** @var RuleOptionArray $ruleOption */
                    $ruleOption = $participation->getRule()->getRuleOption();
                    $options = $ruleOption->getOptions();
                    shuffle($options);
                    $options = array_slice(
                        $options,
                        0,
                        rand($ruleOption->getRandomCountMin(), $ruleOption->getRandomCountMax())
                    );
                    $participationValue->setOptions($options);
                    $participation->addChallengeParticipationValue($participationValue);
                }
                $em->persist($participationValue);
            }
        }

        $em->flush();

        return $this->redirectToRoute('challenge_edit', ['id' => $challenge->getId()]);
    }

    /**
     * @Route("/{id}/finish", name="challenge_finish", requirements={"id"="\d+"})
     */
    public function finishAction(Challenge $challenge)
    {
        $em = $this->getDoctrine()->getManager();
        $challenge->setState(EnumChallengeStateType::CHALLENGE_STATE_FINISHED);
        $challenge->setDateEnded(new \DateTime());
    }

    /**
     * Displays a form to edit an existing challenge entity.
     *
     * @Route("/{id}/edit", name="challenge_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Challenge $challenge)
    {
        $participation = new ChallengeParticipation();
        $participation->setChallenge($challenge);
        $form = $this->createForm(ChallengeParticipationType::class, $participation);
        $form->remove('challenge');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($participation->getPlayers()->isEmpty()) {
                foreach ($challenge->getPlayers() as $player) {
                    $participation->addPlayer($player);
                }
            }

            foreach ($form->get('rule')->getData() as $rule) {
                $challengeParticipation = clone $participation;
                $challengeParticipation->setRule($rule);
                $em->persist($challengeParticipation);
            }
            $em->flush();

            return $this->redirectToRoute('challenge_edit', array('id' => $challenge->getId()));
        }

        return $this->render(
            'challenge/edit.html.twig',
            array(
                'challenge' => $challenge,
                'form'      => $form->createView(),
            )
        );
    }

    /**
     * Deletes a challenge entity.
     *
     * @Route("/{id}", name="challenge_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Challenge $challenge)
    {
        $form = $this->createDeleteForm($challenge);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($challenge);
            $em->flush();
        }

        return $this->redirectToRoute('challenge_index');
    }

    /**
     * Creates a form to delete a challenge entity.
     *
     * @param Challenge $challenge The challenge entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Challenge $challenge)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('challenge_delete', array('id' => $challenge->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * @Route("/participation/{id}/delete", name="challenge_participation_delete")
     */
    public function deleteChallengeParticipationAction(ChallengeParticipation $participation)
    {
        $challenge = $participation->getChallenge();
        $em = $this->getDoctrine()->getManager();
        $em->remove($participation);
        $em->flush();

        return $this->redirectToRoute('challenge_edit', ['id' => $challenge->getId()]);
    }

    /**
     * @Route("/{id}/show/rules", name="challenge_show_rules", requirements={"id"="\d+"})
     * @Method({"GET"})
     */
    public function getChallengeRulesAction(Challenge $challenge)
    {
        $html = $this->renderView('challenge/_rules.html.twig', ['challenge' => $challenge]);

        return JsonResponse::create(['html' => $html, 'code' => 200]);
    }

    /**
     * @Route("/validate/{id}", name="challenge_validate", requirements={"id"="\d+"}, defaults={"id"=0})
     * @Method({"GET"})
     */
    public function validateChallengeAction(Request $request, ChallengeParticipationValue $value = null)
    {
        if (null === $value) {
            throw new NotFoundHttpException();
        }

        $state = $request->query->get('state', 'fail');
        $em = $this->getDoctrine()->getManager();
        if ($state === 'success') {
            if ($value->getSuccess() === 1) {
                $value->setSuccess(0);
            } else {
                $value->setSuccess(1);
            }

        } else {
            if ($value->getSuccess() === -1) {
                $value->setSuccess(0);
            } else {
                $value->setSuccess(-1);
            }

        }
        $em->flush();

        return JsonResponse::create(['code' => 200, 'state' => $value->getSuccess()]);
    }

    /**
     * @Route("/{id}/results", name="challenge_results")
     */
    public function getResultsAction(Request $request, Challenge $challenge)
    {
        $players = [];
        foreach ($challenge->getPlayers() as $player) {
            $players[$player->getId()] = ['player' => $player, 'points' => 0];
        }

        foreach ($challenge->getChallengeParticipations() as $participation) {
            foreach ($participation->getChallengeParticipationValues() as $value) {
                $points = 0;
                if ($value->getSuccess() == 1) {
                    $points = $value->getChallengeParticipation()->getRule()->getRuleOption()->getSuccessPoints();
                    if ($value->getChallengeParticipation()->getRule()->getRuleOption()->getInversedSuccess()) {
                        $points = $value->getChallengeParticipation()->getRule()->getRuleOption()->getFailurePoints();
                    }

                } elseif ($value->getSuccess() == -1) {
                    $points = $value->getChallengeParticipation()->getRule()->getRuleOption()->getFailurePoints();
                    if ($value->getChallengeParticipation()->getRule()->getRuleOption()->getInversedSuccess()) {
                        $points = $value->getChallengeParticipation()->getRule()->getRuleOption()->getSuccessPoints();
                    }
                }
                $players[$value->getPlayer()->getId()]['points'] += $points;
            }
        }

        usort(
            $players,
            function ($a, $b) {
                return ($a['points'] < $b['points']);
            }
        );

        $position = 1;
        $increment = 1;
        $lastPoints = null;
        foreach ($players as $key => $player) {
            if ($lastPoints !== null && $lastPoints != $player['points']) {
                $position += $increment;
                $increment = 1;
            } elseif ($lastPoints !== null) {
                $increment++;
            }
            $players[$key]['position'] = $position;
            $lastPoints = $player['points'];
        }

        if ($request->isXmlHttpRequest()) {
            return JsonResponse::create(
                ['html' => $this->renderView('challenge/_results.html.twig', ['players' => $players])]
            );
        }


        return $this->render('challenge/_results.html.twig', ['players' => $players]);
    }
}
