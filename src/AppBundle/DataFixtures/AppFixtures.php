<?php

namespace AppBundle\DataFixtures;


use AppBundle\Entity\Game;
use AppBundle\Entity\Player;
use AppBundle\Entity\Rule;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $players = ['bisb', 'snake', 'bisti', 'tieuh', 'krower', 'eren'];
        $games = ['Realm Royale', 'PUBG'];
        $rules = [
            [
                'name'   => 'Take a fight in the first zone',
                'points' => 0,
            ],
            [
                'name'   => 'Drop Alone',
                'points' => 0,
            ],
            [
                'name'   => 'Restricted arsenal',
                'points' => 0,
            ],
            [
                'name'   => 'Kill a youtube / twitch player',
                'points' => 0,
            ],
        ];

        $entGames = [];

        foreach ($games as $data) {
            $game = new Game();
            $game->setName($data);
            $manager->persist($game);
            $entGames[] = $game;
        }

        foreach ($rules as $data) {
            $rule = (new Rule())
                ->setName($data['name'])
                ->setPoints($data['points']);
            foreach ($entGames as $entGame) {
                $rule->setGame($entGame);
            }
            $manager->persist($rule);
        }

        foreach ($players as $data) {
            $player = new Player();
            $player->setUsername($data);
            $manager->persist($player);
        }

        $manager->flush();
    }
}