<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ChallengeParticipationValue
 *
 * @ORM\Table(name="challenge_participation_value")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ChallengeParticipationValueRepository")
 */
class ChallengeParticipationValue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var array
     * @ORM\Column(type="array", nullable=true)
     */
    private $options;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $success = 0;

    /**
     * @var ChallengeParticipation
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ChallengeParticipation", inversedBy="challengeParticipationValues")
     */
    private $challengeParticipation;

    /**
     * @var Player
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Player", inversedBy="challengeParticipationValues")
     */
    private $player;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set options
     *
     * @param array $options
     *
     * @return ChallengeParticipationValue
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set challengeParticipation
     *
     * @param \AppBundle\Entity\ChallengeParticipation $challengeParticipation
     *
     * @return ChallengeParticipationValue
     */
    public function setChallengeParticipation(\AppBundle\Entity\ChallengeParticipation $challengeParticipation = null)
    {
        $this->challengeParticipation = $challengeParticipation;

        return $this;
    }

    /**
     * Get challengeParticipation
     *
     * @return \AppBundle\Entity\ChallengeParticipation
     */
    public function getChallengeParticipation()
    {
        return $this->challengeParticipation;
    }

    /**
     * Set player
     *
     * @param \AppBundle\Entity\Player $player
     *
     * @return ChallengeParticipationValue
     */
    public function setPlayer(\AppBundle\Entity\Player $player = null)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * Get player
     *
     * @return \AppBundle\Entity\Player
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * Set success
     *
     * @param integer $success
     *
     * @return ChallengeParticipationValue
     */
    public function setSuccess($success)
    {
        $this->success = $success;

        return $this;
    }

    /**
     * Get success
     *
     * @return integer
     */
    public function getSuccess()
    {
        return $this->success;
    }
}
