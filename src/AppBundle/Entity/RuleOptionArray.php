<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RuleOptionArray
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RuleOptionArrayRepository")
 */
class RuleOptionArray extends RuleOption
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var array
     * @ORM\Column(type="array")
     */
    private $options;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $random = false;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $randomCountMin = 1;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $randomCountMax = 10;

    /**
     * RuleOptionArray constructor.
     */
    public function __construct()
    {
        $this->options = [];
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set options
     *
     * @param array $options
     *
     * @return RuleOptionArray
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set random
     *
     * @param boolean $random
     *
     * @return RuleOptionArray
     */
    public function setRandom($random)
    {
        $this->random = $random;

        return $this;
    }

    /**
     * Get random
     *
     * @return boolean
     */
    public function getRandom()
    {
        return $this->random;
    }

    /**
     * Set randomCountMin
     *
     * @param integer $randomCountMin
     *
     * @return RuleOptionArray
     */
    public function setRandomCountMin($randomCountMin)
    {
        $this->randomCountMin = $randomCountMin;

        return $this;
    }

    /**
     * Get randomCountMin
     *
     * @return integer
     */
    public function getRandomCountMin()
    {
        return $this->randomCountMin;
    }

    /**
     * Set randomCountMax
     *
     * @param integer $randomCountMax
     *
     * @return RuleOptionArray
     */
    public function setRandomCountMax($randomCountMax)
    {
        $this->randomCountMax = $randomCountMax;

        return $this;
    }

    /**
     * Get randomCountMax
     *
     * @return integer
     */
    public function getRandomCountMax()
    {
        return $this->randomCountMax;
    }
}
