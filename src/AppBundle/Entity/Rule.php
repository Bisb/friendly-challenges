<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Rule
 *
 * @ORM\Table(name="rule")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RuleRepository")
 */
class Rule
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var RuleOption
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\RuleOption", mappedBy="rule", cascade={"persist", "remove"})
     */
    private $ruleOption;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ChallengeParticipation", mappedBy="rule")
     */
    private $challengeParticipations;

    /**
     * @var Game
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Game", inversedBy="rules")
     * @ORM\JoinColumn(nullable=false)
     */
    private $game;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Challenge", mappedBy="rules")
     */
    private $challenges;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->challenges = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Rule
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set game
     *
     * @param \AppBundle\Entity\Game $game
     *
     * @return Rule
     */
    public function setGame(\AppBundle\Entity\Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \AppBundle\Entity\Game
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Add challenge
     *
     * @param \AppBundle\Entity\Challenge $challenge
     *
     * @return Rule
     */
    public function addChallenge(\AppBundle\Entity\Challenge $challenge)
    {
        $this->challenges[] = $challenge;

        return $this;
    }

    /**
     * Remove challenge
     *
     * @param \AppBundle\Entity\Challenge $challenge
     */
    public function removeChallenge(\AppBundle\Entity\Challenge $challenge)
    {
        $this->challenges->removeElement($challenge);
    }

    /**
     * Get challenges
     *
     * @return \Doctrine\Common\Collections\Collection|Challenge[]
     */
    public function getChallenges()
    {
        return $this->challenges;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Rule
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set ruleOption
     *
     * @param \AppBundle\Entity\RuleOption $ruleOption
     *
     * @return Rule
     */
    public function setRuleOption(\AppBundle\Entity\RuleOption $ruleOption = null)
    {
        $this->ruleOption = $ruleOption;
        $ruleOption->setRule($this);

        return $this;
    }

    /**
     * Get ruleOption
     *
     * @return \AppBundle\Entity\RuleOption
     */
    public function getRuleOption()
    {
        return $this->ruleOption;
    }

    /**
     * Add challengeParticipation
     *
     * @param \AppBundle\Entity\ChallengeParticipation $challengeParticipation
     *
     * @return Rule
     */
    public function addChallengeParticipation(\AppBundle\Entity\ChallengeParticipation $challengeParticipation)
    {
        $this->challengeParticipations[] = $challengeParticipation;

        return $this;
    }

    /**
     * Remove challengeParticipation
     *
     * @param \AppBundle\Entity\ChallengeParticipation $challengeParticipation
     */
    public function removeChallengeParticipation(\AppBundle\Entity\ChallengeParticipation $challengeParticipation)
    {
        $this->challengeParticipations->removeElement($challengeParticipation);
    }

    /**
     * Get challengeParticipations
     *
     * @return \Doctrine\Common\Collections\Collection|ChallengeParticipation[]
     */
    public function getChallengeParticipations()
    {
        return $this->challengeParticipations;
    }
}
