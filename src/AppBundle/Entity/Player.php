<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Player
 *
 * @ORM\Table(name="player")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PlayerRepository")
 */
class Player
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $username;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $points = 0;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ChallengeParticipationValue", mappedBy="player")
     */
    private $challengeParticipationValues;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Challenge", mappedBy="players")
     */
    private $challenges;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\ChallengeParticipation", mappedBy="players")
     */
    private $challengeParticipations;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->challenges = new \Doctrine\Common\Collections\ArrayCollection();
        $this->challengeParticipations = new ArrayCollection();
        $this->challengeParticipationValues = new ArrayCollection();
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return Player
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Add challenge
     *
     * @param \AppBundle\Entity\Challenge $challenge
     *
     * @return Player
     */
    public function addChallenge(\AppBundle\Entity\Challenge $challenge)
    {
        $this->challenges[] = $challenge;

        return $this;
    }

    /**
     * Remove challenge
     *
     * @param \AppBundle\Entity\Challenge $challenge
     */
    public function removeChallenge(\AppBundle\Entity\Challenge $challenge)
    {
        $this->challenges->removeElement($challenge);
    }

    /**
     * Get challenges
     *
     * @return \Doctrine\Common\Collections\Collection|Challenge[]
     */
    public function getChallenges()
    {
        return $this->challenges;
    }

    public function __toString()
    {
        return $this->username;
    }

    /**
     * Add challengeParticipation
     *
     * @param \AppBundle\Entity\ChallengeParticipation $challengeParticipation
     *
     * @return Player
     */
    public function addChallengeParticipation(\AppBundle\Entity\ChallengeParticipation $challengeParticipation)
    {
        $this->challengeParticipations[] = $challengeParticipation;

        return $this;
    }

    /**
     * Remove challengeParticipation
     *
     * @param \AppBundle\Entity\ChallengeParticipation $challengeParticipation
     */
    public function removeChallengeParticipation(\AppBundle\Entity\ChallengeParticipation $challengeParticipation)
    {
        $this->challengeParticipations->removeElement($challengeParticipation);
    }

    /**
     * Get challengeParticipations
     *
     * @return \Doctrine\Common\Collections\Collection|ChallengeParticipation[]
     */
    public function getChallengeParticipations()
    {
        return $this->challengeParticipations;
    }

    /**
     * Add challengeParticipationValue
     *
     * @param \AppBundle\Entity\ChallengeParticipationValue $challengeParticipationValue
     *
     * @return Player
     */
    public function addChallengeParticipationValue(\AppBundle\Entity\ChallengeParticipationValue $challengeParticipationValue)
    {
        $this->challengeParticipationValues[] = $challengeParticipationValue;

        return $this;
    }

    /**
     * Remove challengeParticipationValue
     *
     * @param \AppBundle\Entity\ChallengeParticipationValue $challengeParticipationValue
     */
    public function removeChallengeParticipationValue(\AppBundle\Entity\ChallengeParticipationValue $challengeParticipationValue)
    {
        $this->challengeParticipationValues->removeElement($challengeParticipationValue);
    }

    /**
     * Get challengeParticipationValues
     *
     * @return \Doctrine\Common\Collections\Collection|ChallengeParticipationValue[]
     */
    public function getChallengeParticipationValues()
    {
        return $this->challengeParticipationValues;
    }

    /**
     * Set points
     *
     * @param integer $points
     *
     * @return Player
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return integer
     */
    public function getPoints()
    {
        return $this->points;
    }
}
