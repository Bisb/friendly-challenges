<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ChallengeParticipation
 *
 * @ORM\Table(name="challenge_participation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ChallengeParticipationRepository")
 */
class ChallengeParticipation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $random = false;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ChallengeParticipationValue", mappedBy="challengeParticipation")
     */
    private $challengeParticipationValues;

    /**
     * @var Challenge
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Challenge", inversedBy="challengeParticipations")
     */
    private $challenge;

    /**
     * @var Rule
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Rule", inversedBy="challengeParticipations")
     */
    private $rule;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Player", inversedBy="challengeParticipations")
     */
    private $players;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->players = new \Doctrine\Common\Collections\ArrayCollection();
        $this->challengeParticipationValues = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set challenge
     *
     * @param \AppBundle\Entity\Challenge $challenge
     *
     * @return ChallengeParticipation
     */
    public function setChallenge(\AppBundle\Entity\Challenge $challenge = null)
    {
        $this->challenge = $challenge;

        return $this;
    }

    /**
     * Get challenge
     *
     * @return \AppBundle\Entity\Challenge
     */
    public function getChallenge()
    {
        return $this->challenge;
    }

    /**
     * Set rule
     *
     * @param \AppBundle\Entity\Rule $rule
     *
     * @return ChallengeParticipation
     */
    public function setRule(\AppBundle\Entity\Rule $rule = null)
    {
        $this->rule = $rule;

        return $this;
    }

    /**
     * Get rule
     *
     * @return \AppBundle\Entity\Rule
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * Add player
     *
     * @param \AppBundle\Entity\Player $player
     *
     * @return ChallengeParticipation
     */
    public function addPlayer(\AppBundle\Entity\Player $player)
    {
        $this->players[] = $player;

        return $this;
    }

    /**
     * Remove player
     *
     * @param \AppBundle\Entity\Player $player
     */
    public function removePlayer(\AppBundle\Entity\Player $player)
    {
        $this->players->removeElement($player);
    }

    /**
     * @param array $players
     * @return $this
     */
    public function setPlayers($players)
    {
        $this->players = new ArrayCollection();

        foreach ($players as $player) {
            $this->addPlayer($player);
        }

        return $this;
    }

    /**
     * Get players
     *
     * @return \Doctrine\Common\Collections\Collection|Player[]
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * Set random
     *
     * @param boolean $random
     *
     * @return ChallengeParticipation
     */
    public function setRandom($random)
    {
        $this->random = $random;

        return $this;
    }

    /**
     * Get random
     *
     * @return boolean
     */
    public function getRandom()
    {
        return $this->random;
    }

    /**
     * Add challengeParticipationValue
     *
     * @param \AppBundle\Entity\ChallengeParticipationValue $challengeParticipationValue
     *
     * @return ChallengeParticipation
     */
    public function addChallengeParticipationValue(\AppBundle\Entity\ChallengeParticipationValue $challengeParticipationValue)
    {
        $this->challengeParticipationValues[] = $challengeParticipationValue;
        $challengeParticipationValue->setChallengeParticipation($this);

        return $this;
    }

    /**
     * Remove challengeParticipationValue
     *
     * @param \AppBundle\Entity\ChallengeParticipationValue $challengeParticipationValue
     */
    public function removeChallengeParticipationValue(\AppBundle\Entity\ChallengeParticipationValue $challengeParticipationValue)
    {
        $this->challengeParticipationValues->removeElement($challengeParticipationValue);
    }

    /**
     * Get challengeParticipationValues
     *
     * @return \Doctrine\Common\Collections\Collection|ChallengeParticipationValue[]
     */
    public function getChallengeParticipationValues()
    {
        return $this->challengeParticipationValues;
    }
}
