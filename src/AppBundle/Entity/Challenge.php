<?php

namespace AppBundle\Entity;

use AppBundle\DBAL\EnumChallengeStateType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Challenge
 *
 * @ORM\Table(name="challenge")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ChallengeRepository")
 */
class Challenge
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $name;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $dateCreated;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateEnded;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateStarted;

    /**
     * @ORM\Column(type="enum_challenge_state")
     */
    private $state = EnumChallengeStateType::CHALLENGE_STATE_PREPARING;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ChallengeParticipation", mappedBy="challenge")
     */
    private $challengeParticipations;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Game", inversedBy="challenges")
     * @ORM\JoinColumn(nullable=false)
     */
    private $game;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Rule", inversedBy="challenges")
     */
    private $rules;

    /**
     * @var ArrayCollection
     * @Assert\Count(min="2")
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Player", inversedBy="challenges")
     */
    private $players;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rules = new \Doctrine\Common\Collections\ArrayCollection();
        $this->players = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dateCreated = new \DateTime();
    }

    /**
     * Get dateEnded
     *
     * @return \DateTime
     */
    public function getDateEnded()
    {
        return $this->dateEnded;
    }

    /**
     * Set dateEnded
     *
     * @param \DateTime $dateEnded
     *
     * @return Challenge
     */
    public function setDateEnded($dateEnded)
    {
        $this->dateEnded = $dateEnded;

        return $this;
    }

    /**
     * Get dateStarted
     *
     * @return \DateTime
     */
    public function getDateStarted()
    {
        return $this->dateStarted;
    }

    /**
     * Set dateStarted
     *
     * @param \DateTime $dateStarted
     *
     * @return Challenge
     */
    public function setDateStarted($dateStarted)
    {
        $this->dateStarted = $dateStarted;

        return $this;
    }

    /**
     * Get game
     *
     * @return \AppBundle\Entity\Game
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Set game
     *
     * @param \AppBundle\Entity\Game $game
     *
     * @return Challenge
     */
    public function setGame(\AppBundle\Entity\Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Add rule
     *
     * @param \AppBundle\Entity\Rule $rule
     *
     * @return Challenge
     */
    public function addRule(\AppBundle\Entity\Rule $rule)
    {
        $this->rules[] = $rule;

        return $this;
    }

    /**
     * Remove rule
     *
     * @param \AppBundle\Entity\Rule $rule
     */
    public function removeRule(\AppBundle\Entity\Rule $rule)
    {
        $this->rules->removeElement($rule);
    }

    /**
     * Get rules
     *
     * @return \Doctrine\Common\Collections\Collection|Rule[]
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * Add player
     *
     * @param \AppBundle\Entity\Player $player
     *
     * @return Challenge
     */
    public function addPlayer(\AppBundle\Entity\Player $player)
    {
        $this->players[] = $player;

        return $this;
    }

    /**
     * Remove player
     *
     * @param \AppBundle\Entity\Player $player
     */
    public function removePlayer(\AppBundle\Entity\Player $player)
    {
        $this->players->removeElement($player);
    }

    /**
     * Get players
     *
     * @return \Doctrine\Common\Collections\Collection|Player[]
     */
    public function getPlayers()
    {
        return $this->players;
    }

    public function __toString()
    {
        if ($this->getName()) {
            return $this->getName();
        }

        return '#'.$this->getId().' '.$this->getDateCreated()->format('H:i:s d/m/Y');
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return Challenge
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return Challenge
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Add challengeParticipation
     *
     * @param \AppBundle\Entity\ChallengeParticipation $challengeParticipation
     *
     * @return Challenge
     */
    public function addChallengeParticipation(\AppBundle\Entity\ChallengeParticipation $challengeParticipation)
    {
        $this->challengeParticipations[] = $challengeParticipation;

        return $this;
    }

    /**
     * Remove challengeParticipation
     *
     * @param \AppBundle\Entity\ChallengeParticipation $challengeParticipation
     */
    public function removeChallengeParticipation(\AppBundle\Entity\ChallengeParticipation $challengeParticipation)
    {
        $this->challengeParticipations->removeElement($challengeParticipation);
    }

    /**
     * Get challengeParticipations
     *
     * @return \Doctrine\Common\Collections\Collection|ChallengeParticipation[]
     */
    public function getChallengeParticipations()
    {
        return $this->challengeParticipations;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Challenge
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
