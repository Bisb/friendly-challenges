<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * RuleOption
 *
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"boolean" = "RuleOptionBoolean", "array" = "RuleOptionArray", "integer" = "RuleOptionInteger"})
 * @ORM\Table(name="rule_option")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RuleOptionRepository")
 */
abstract class RuleOption
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $successPoints = 100;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $failurePoints = -100;

    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $defaultPoints = 0;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $inversedSuccess = false;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(type="string")
     */
    private $message;

    /**
     * @var Rule
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Rule", inversedBy="ruleOption")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rule;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set successPoints
     *
     * @param integer $successPoints
     *
     * @return RuleOption
     */
    public function setSuccessPoints($successPoints)
    {
        $this->successPoints = $successPoints;

        return $this;
    }

    /**
     * Get successPoints
     *
     * @return integer
     */
    public function getSuccessPoints()
    {
        return $this->successPoints;
    }

    /**
     * Set failurePoints
     *
     * @param integer $failurePoints
     *
     * @return RuleOption
     */
    public function setFailurePoints($failurePoints)
    {
        $this->failurePoints = $failurePoints;

        return $this;
    }

    /**
     * Get failurePoints
     *
     * @return integer
     */
    public function getFailurePoints()
    {
        return $this->failurePoints;
    }

    /**
     * Set defaultPoints
     *
     * @param integer $defaultPoints
     *
     * @return RuleOption
     */
    public function setDefaultPoints($defaultPoints)
    {
        $this->defaultPoints = $defaultPoints;

        return $this;
    }

    /**
     * Get defaultPoints
     *
     * @return integer
     */
    public function getDefaultPoints()
    {
        return $this->defaultPoints;
    }

    /**
     * Set inversedSuccess
     *
     * @param boolean $inversedSuccess
     *
     * @return RuleOption
     */
    public function setInversedSuccess($inversedSuccess)
    {
        $this->inversedSuccess = $inversedSuccess;

        return $this;
    }

    /**
     * Get inversedSuccess
     *
     * @return boolean
     */
    public function getInversedSuccess()
    {
        return $this->inversedSuccess;
    }

    /**
     * Set rule
     *
     * @param \AppBundle\Entity\Rule $rule
     *
     * @return RuleOption
     */
    public function setRule(\AppBundle\Entity\Rule $rule)
    {
        $this->rule = $rule;

        return $this;
    }

    /**
     * Get rule
     *
     * @return \AppBundle\Entity\Rule
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return RuleOption
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }
}
